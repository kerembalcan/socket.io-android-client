package com.example.kerembalcan.socketdenemesi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    String abc;
    String a[];
    private List<String> listIds = new ArrayList<String>();
    private List<String> listNames = new ArrayList<String>();
    private List<String> listEmails = new ArrayList<String>();
    private String[] ids;
    private String[] names;
    private String[] emails;

    public static final String JSON_URL = "http://mangiorpi.herokuapp.com/api/user";

    private Button buttonGet;

    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Socket socket = null;
        try {
            socket = IO.socket("http://mangiorpi.herokuapp.com");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        final Socket finalSocket = socket;
        assert socket != null;
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.e(finalSocket.toString(), finalSocket.connected() + "");
            }

        }).on("user:created", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        String username;
                        String firstname;
                        String email;
                        try {
                            username = data.getString("userName");
                            firstname = data.getString("firstName");
                            email = data.getString("email");
                        } catch (JSONException e) {
                            return;
                        }

                        // add the message to view
                        addUserWithSocket(username, firstname, email);
                    }
                });
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
            }

        });
        socket.connect();
        Log.e(socket.toString(), socket.connected() + "");

        setContentView(R.layout.activity_main);
        buttonGet = (Button) findViewById(R.id.buttonGet);
        buttonGet.setOnClickListener((View.OnClickListener) this);
        listView = (ListView) findViewById(R.id.listView);

    }

/*
    private Emitter.Listener user = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String firstname;
                    String email;
                    try {
                        username = data.getString("userName");
                        firstname = data.getString("firstName");
                        email = data.getString("email");
                        Log.e("kerem", email);
                    } catch (JSONException e) {
                        return;
                    }

                    // add the message to view
                    Log.e("kerem", email);
                    addUserWithSocket(username, firstname, email);
                }
            });
        }

    };
    */

/*    private void sendRequest() {

        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    } */

    private void sendReq() {

        JsonArrayRequest req_user = new JsonArrayRequest(JSON_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject person = (JSONObject) response
                                        .get(i);
                                listIds.add(person.getString("userName"));
                                listNames.add(person.getString("firstName"));
                                listEmails.add(person.getString("email"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req_user);
        ids = listIds.toArray(new String[0]);
        names = listNames.toArray(new String[0]);
        emails = listEmails.toArray(new String[0]);
        CustomList cl = new CustomList(this, ids, names, emails);
        listView.setAdapter(cl);
    }

   /* private void showJSON(String json) {
        ParseJSON pj = new ParseJSON(json);
        Log.e("1", pj.toString());
        pj.parseJSON();
        Log.e("2", pj.toString());
        CustomList cl = new CustomList(this, ParseJSON.ids, ParseJSON.names, ParseJSON.emails);
        listView.setAdapter(cl);
    }*/

    private void addUserWithSocket(String username, String firstname, String mail) {
        listIds.add(username);
        listNames.add(firstname);
        listEmails.add(mail);
        ids = listIds.toArray(new String[0]);
        names = listNames.toArray(new String[0]);
        emails = listEmails.toArray(new String[0]);

        CustomList cl = new CustomList(this, ids, names, emails);
        listView.setAdapter(cl);

    }

    @Override
    public void onClick(View v) {
        sendReq();
    }

}
